import re
import sys
import logging

import numpy as np
import pandas as pd

import lda
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.manifold import TSNE

from pymystem3 import Mystem
from ml.text_preprocessor import Preproc

from bokeh.plotting import figure, output_file, show
from bokeh.models import Label
from bokeh.io import output_notebook
import matplotlib.colors as mcolors

import matplotlib.pyplot as plt

"""
Todo
1. Input
    + array of sentences
    
2. Preprocessing
    + delete punctiation
    + lemmatization
    
3. LDA
    + CountVectorizer
         + lower
         + delete stopwords
         + n-gramm's
    + LDA applying
    
4. Visualisation
    + t-sne
    - visualization


"""



grammems = {
    "A": "прилагательное",
    "ADV": "наречие",
    "ADVPRO": "местоименное наречие",
    "ANUM": "числительное-прилагательное",
    "APRO": "местоимение-прилагательное",
    "COM": "часть композита - сложного слова",
    "CONJ": "союз",
    "INTJ": "междометие",
    "NUM": "числительное",
    "PART": "частица",
    "PR": "предлог",
    "S": "существительное",
    "SPRO": "местоимение-существительное",
    "V": "глагол",
}


class TopicModeller:

    n_topics = 3  # number of topics
    n_iter = 500  # number of iterations
    n_top_words = 8
    speech_parts = {"A", "ADV", "S", "V"}

    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")


    def __init__(self, config=None):
        self.logger = logging.getLogger("TopicModeller")
        self.c = config

        try:
            f = open("stopwords.txt")
            self.stopwords = f.read().splitlines()
            self.logger.info("stopwords ok")
        except Exception as e:
            self.stopwords = []
            self.logger.warning(e, exc_info=True)

        self.lemmmatizator = Mystem()
        self.vectorizer = CountVectorizer(min_df=3, stop_words=self.stopwords, ngram_range=(1, 3))
        self.lda_model = lda.LDA(n_topics=self.n_topics, n_iter=self.n_iter, random_state=100)

        self.logger.info("Initialized")
        pass

    def lemm(self, corpus):
        for sentence in corpus:
            sentence_lmd = []
            result = self.lemmmatizator.analyze(sentence)
            for word in result:
                if "analysis" in word and len(word["analysis"]):
                    try:
                        word_lmd = word["analysis"][0]["lex"]
                        if len(self.speech_parts):
                            speech_part = re.split("[,=]+", word["analysis"][0]["gr"])
                            if speech_part[0] not in self.speech_parts:
                                continue
                        sentence_lmd.append(word_lmd)
                    except Exception:
                        pass
            if len(sentence_lmd):
                yield " ".join(sentence_lmd)

    def preprocess(self, corpus):
        """Предобработка предложения - удаление символов"""
        for sentence in corpus:
            try:
                sentence = self.REPLACE_NO_SPACE.sub("", sentence)
            except:
                sentence = ""
            yield(sentence)

    def model(self, corpus):
        corpus = list(self.preprocess(corpus))
        corpus_lemmed = list(self.lemm(corpus))

        cvz = self.vectorizer.fit_transform(corpus_lemmed)
        X_topics = self.lda_model.fit_transform(cvz)

        _lda_keys = []
        for i in range(X_topics.shape[0]):
            _lda_keys += X_topics[i].argmax(),

        keywords = np.array(self.vectorizer.get_feature_names())
        topic_keywords = []
        for topic_weights in self.lda_model.components_:
            top_keyword_locs = (-topic_weights).argsort()[:self.n_top_words]
            topic_keywords.append(keywords.take(top_keyword_locs))



        #Dimension reduction
        tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')
        tsne_lda = tsne_model.fit_transform(X_topics)


        """
        # Plot the Topic Clusters using Bokeh
        mycolors = np.array([color for name, color in mcolors.TABLEAU_COLORS.items()])
        print(mycolors)
        plot = figure(title="t-SNE Clustering of {} LDA Topics".format(self.n_topics),
                      plot_width=900, plot_height=700)
        plot.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], color=mycolors[self.n_topics], marker=_lda_keys)
        show(plot)
        """
        fig = plt.figure()
        ax = plt.axes()

        ax.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], c=_lda_keys, alpha=0.3, ) #label=
        ax.set(title="Scatter plot example",
               xlabel='x', ylabel='y')
        plt.show()


        print("_lda_keys")
        print(len(_lda_keys))
        print(_lda_keys)

        print("keywords")
        print(len(keywords))
        print(keywords)

        print("self.vectorizer.vocabulary_")
        print(len(self.vectorizer.vocabulary_))
        print(self.vectorizer.vocabulary_)
















if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)
    p = Preproc()

    c = [
        "Французская революция - крупнейшая трансформация социальной и политической системы Франции",
        "Впервые с начала Французкой революции в печати стали открыто обсуждать возможность установления республики",
        "Юрий спасибо Никогда не пишу комментарии но тут не сдержался Вы большой молодец",
        "Юрий продолжайте снимать такие крайне полезные фильмы для народа страны и самого государства",
        "Юра вы со своей командой часто делаете потрясающие вещи это заслуживает уважения и спасибо вам"
    ]

    d3 = pd.read_csv("./tests/d3.csv", sep=";")
    d3["label"]="d3"
    cmnts_zarrubin = pd.read_csv("./tests/zarrubin.csv", sep=",", index_col="ind")
    cmnts_zarrubin["label"] = "z"
    cmnts_vdud = pd.read_csv("./tests/vdud_kolyma.csv", sep=",", index_col="ind")
    cmnts_vdud["label"] = "d"

    data = d3#pd.concat([d3, cmnts_zarrubin,cmnts_vdud])

    #data = pd.read_csv("./tests/big/dud.csv")





    c = data["comment"].values

    tm = TopicModeller()
    #tm.model(c)

    n_topics = 10  # number of topics
    n_iter = 500  # number of iterations
    n_top_words =5

    speech_parts = {"A",  "S", "V"}
    stopwords = ["а", "и", "я", "раз"]

    #------------------------------------------------------------
    corpus = c
    lemmmatizator = Mystem()
    vectorizer = CountVectorizer(min_df=4, stop_words=stopwords, ngram_range=(1, 3))
    lda_model = lda.LDA(n_topics=n_topics, n_iter=n_iter, random_state=10)

    corpus = list(tm.preprocess(corpus))
    corpus_lemmed = list(tm.lemm(corpus))

    cvz = vectorizer.fit_transform(corpus_lemmed)
    X_topics = lda_model.fit_transform(cvz)

    _lda_keys = []
    for i in range(X_topics.shape[0]):
        _lda_keys += X_topics[i].argmax(),

    keywords = np.array(vectorizer.get_feature_names())
    topic_keywords = []
    for topic_weights in lda_model.components_:
        top_keyword_locs = (-topic_weights).argsort()[:n_top_words]
        topic_keywords.append(keywords.take(top_keyword_locs))



    # Dimension reduction
    threshold = 0.5
    _idx = np.amax(X_topics, axis=1) > threshold  # idx of doc that above the threshold

    X_topics = X_topics[_idx]
    _lda_keys = np.array(_lda_keys)[_idx]

    tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')
    tsne_lda = tsne_model.fit_transform(X_topics)



    topic_coord = np.empty((X_topics.shape[1], 2)) * np.nan
    print(_lda_keys)
    for _idx, topic_num in enumerate(_lda_keys):
        print(_idx)
        print(topic_num)
        topic_coord[topic_num] = tsne_lda[_idx]

        if not np.isnan(topic_coord).any():
            break







    """
    # Plot the Topic Clusters using Bokeh
    mycolors = np.array([color for name, color in mcolors.TABLEAU_COLORS.items()])
    print(mycolors)
    plot = figure(title="t-SNE Clustering of {} LDA Topics".format(self.n_topics),
                  plot_width=900, plot_height=700)
    plot.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], color=mycolors[self.n_topics], marker=_lda_keys)
    show(plot)
    """

    """
    Для графика
    1. Взять данные осей x и y      tsne_lda[:, 0], tsne_lda[:, 1]
    2. Взять группу                 
    3. Взять значение 
    
    """



    fig = plt.figure()
    ax = plt.axes()

    ax.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], c=_lda_keys, alpha=0.3, )  # label=
    ax.set(title="Scatter plot example",
           xlabel='x', ylabel='y')




    for i, txt in enumerate(topic_keywords):
        ax.annotate(" \n".join(txt), (topic_coord[:, 0][i], topic_coord[:, 1][i]))
    plt.show()




    """
    1. Применить tsne к components
    2. Показать точки  - слова и разделить их по группам
    3. Выводить некоторый топ точек
    
    
    1. Применить tsne к _doc_topic
    2. Показать точки  - Комментарии
    3. Выводить некоторый топ точек
    
    
    
    """










