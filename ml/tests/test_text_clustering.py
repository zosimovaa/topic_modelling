import pandas as pd
from ml.text_clustering import TxtCluster




cmnts_d3 = pd.read_csv("d3.csv", sep=";")
cmnts_zarrubin = pd.read_csv("zarrubin.csv", sep=",", index_col="ind")
cmnts_vdud = pd.read_csv("vdud_kolyma.csv", sep=",", index_col="ind")



data = pd.concat([cmnts_d3, cmnts_zarrubin, cmnts_vdud])

ml = TxtCluster()

res = ml.processing(cmnts_d3)
