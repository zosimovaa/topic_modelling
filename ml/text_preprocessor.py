import re
import logging

import pymorphy2
from pymystem3 import Mystem
import lda
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.datasets import fetch_20newsgroups


class Preproc:
    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")

    def __init__(self):
        self.logger = logging.getLogger("TopicModeller")
        self.lemm1 = pymorphy2.MorphAnalyzer()
        self.lemm2 = Mystem()
        self.logger.debug("Initialized")

        try:
            f = open("stopwords.txt")
            self.stopwords = f.read().splitlines()
            self.logger.info("stopwords ok")
        except Exception as e:
            self.stopwords = [];
            self.logger.info("stopwords does not exists")
            raise e

    def __lower(self, corp):
        corp = [sent.lower() for sent in corp]
        return corp

    def __punctuation(self, corp):
        corp = [self.REPLACE_NO_SPACE.sub("", sent) for sent in corp]
        return corp

    def __lemm(self, corp, lemm_type=False):
        for _ in range(len(corp)):
            data = corp[_].split()
            res = []
            for word in data:
                if lemm_type==1:
                    word = self.lemm1.parse(word)  # stemmer.stem(word)
                    word = word[0].normal_form
                elif lemm_type==2:
                    word = self.lemm2.lemmatize(word)  # stemmer.stem(word)
                    word = word[0]

                if word in self.stopwords: continue
                res.append(word)
            corp[_] = " ".join(res)
        return corp

    def processing(self, corpus, lwr=False, punct=False, lemm=False, lemm_type=False):
        if lwr:
            corpus = self.__lower(corpus)

        if punct:
            corpus = self.__punctuation(corpus)

        if lemm:
            corpus = self.__lemm(corpus, lemm_type)

        return corpus

