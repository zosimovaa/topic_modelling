import unittest
import pandas as pd
from yce.yce import *

#o2lxoDR5Srg
#lKPsAUfu1QM
class TestMessageFilters(unittest.TestCase):

    def setUp(self):
        self.yce = YoutubeCommentExtractor('AIzaSyD2kMwfHQImKFOdPVZLKko82NDULrUlbEQ')
        self.yce2 = YoutubeCommentExtractor('aaa')

    def test_youtube(self):
        res = self.yce.get_video_comments("lKPsAUfu1QM", 10)
        self.assertEqual(res.shape[0], 10)

    def test_no_comments(self):
        res = self.yce.get_video_comments("o2lxoDR5Srg", 10)
        self.assertEqual(res.shape[0], 0)

    def test_video_not_found(self):
        self.assertRaises(YCEResponseError, self.yce.get_video_comments, "2lxoDR5Srg", 10)

    def test_bad_api_key(self):
        self.assertRaises(YCEResponseError, self.yce2.get_video_comments, "2lxoDR5Srg", 10)
        try:
            res = self.yce2.get_video_comments("2lxoDR5Srg", 10)
        except Exception as e:
            self.assertEqual(e.__str__(), "Bad Request")


if __name__ == '__main__':
    unittest.main()
