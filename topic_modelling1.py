import re
import sys
import logging

import numpy as np
import pandas as pd
import random

import lda
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer


from sklearn.manifold import TSNE

from pymystem3 import Mystem
from ml.text_preprocessor import Preproc

from bokeh.plotting import figure, output_file, show
from bokeh.models import Label
from bokeh.io import output_notebook
import matplotlib.colors as mcolors

import matplotlib.pyplot as plt



grammems = {
    "A": "прилагательное",
    "ADV": "наречие",
    "ADVPRO": "местоименное наречие",
    "ANUM": "числительное-прилагательное",
    "APRO": "местоимение-прилагательное",
    "COM": "часть композита - сложного слова",
    "CONJ": "союз",
    "INTJ": "междометие",
    "NUM": "числительное",
    "PART": "частица",
    "PR": "предлог",
    "S": "существительное",
    "SPRO": "местоимение-существительное",
    "V": "глагол",
}


class TopicModeller:

    n_topics = 9  # number of topics
    n_iter = 100  # number of iterations
    n_top_words = 8

    vect_min_df = 4
    speech_parts = {"A", "ADV", "S", "V"}

    random_state = 111

    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")

    def __init__(self, config=None):
        """Инициализация 'Топик Моделлёра'"""
        self.logger = logging.getLogger("TopicModeller")
        self.c = config

        try:
            f = open("stopwords.txt")
            self.stopwords = f.read().splitlines()
            self.logger.info("stopwords ok")
        except Exception as e:
            self.stopwords = []
            self.logger.warning(e, exc_info=True)

        self.lemmmatizator = Mystem()
        self.vectorizer = CountVectorizer(min_df=self.vect_min_df,
                                          stop_words=self.stopwords,
                                          ngram_range=(1, 3))

        self.lda_model = lda.LDA(n_topics=self.n_topics,
                                 n_iter=self.n_iter,
                                 random_state=self.random_state)

        self.tsne_model = TSNE(n_components=2,
                               verbose=1,
                               random_state=self.random_state,
                               angle=.99,
                               init='pca')

        self.smpls = None
        self.smpls_original = None
        self.smpls_id_vect = []
        self.smpls1 = []

        self.smpls_id_topics = []

        self.smpls_id_topic_name = []

        self.topic_keywords = []
        self.logger.info("Initialized")


    def fit(self, corpus):

        self.smpls_original = corpus

        self.smpls = list(self._preprocess(self.smpls_original))
        self.smpls = list(self._lemm(self.smpls))


        self.smpls_id_vect = self.vectorizer.fit_transform(self.smpls)
        self.smpls_id_topics = self.lda_model.fit_transform(self.smpls_id_vect)

        self.smpls_id_topic_name = []
        for i in range(self.smpls_id_topics.shape[0]):
            self.smpls_id_topic_name += self.smpls_id_topics[i].argmax(),


        keywords = np.array(self.vectorizer.get_feature_names())
        self.topic_keywords = []
        for topic_weights in self.lda_model.components_:
            top_keyword_locs = (-topic_weights).argsort()[:self.n_top_words]
            self.topic_keywords.append(keywords.take(top_keyword_locs))

        pass


    def get_ids_by_topic(self, topic_name):
        """Возвращает id семплов, которые формирут топик"""
        return np.where(self.smpls_id_topic_name == topic_name)

    def get_smpls_by_topic(self, topic_name, count):
        """Возвращает id семплов, которые формирут топик"""
        res = self.smpls_original[self.get_ids_by_topic(topic_name)]


        idx1 = random.sample(list(range(len(res))), count)
        res = res[idx1]
        print(res)
        return res



    def draw(self):
        # Dimension reduction

        threshold = 0.6
        _idx = np.amax(self.smpls_id_topics, axis=1) > threshold  # idx of doc that above the threshold

        self.smpls_id_topics = self.smpls_id_topics[_idx]
        self.smpls_id_topic_name = np.array(self.smpls_id_topic_name)[_idx]
        self.smpls = np.array(self.smpls)[_idx]
        #self.smpls_original = np.array(self.smpls_original)[_idx]

        tsne_lda = self.tsne_model.fit_transform(self.smpls_id_topics)

        topic_coord = np.empty(( self.smpls_id_topics.shape[1], 2)) * np.nan
        print( self.smpls_id_topic_name)
        for _idx, topic_num in enumerate( self.smpls_id_topic_name):
            topic_coord[topic_num] = tsne_lda[_idx]
            if not np.isnan(topic_coord).any():
                break



        fig = plt.figure()
        ax = plt.axes()

        ax.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], c=self.smpls_id_topic_name, alpha=0.3, )  # label=
        ax.set(title="Scatter plot example",
               xlabel='x', ylabel='y')

        for i, txt in enumerate(self.topic_keywords):
            ann = "\n".join(txt)
            ann2 = "Topic {0}\n{1}".format(i, ann)


            ax.annotate(ann2, (topic_coord[:, 0][i], topic_coord[:, 1][i]))
        plt.show()


        plt.show()



    def _lemm(self, corpus):
        for sentence in corpus:
            sentence_lmd = []
            result = self.lemmmatizator.analyze(sentence)
            for word in result:
                if "analysis" in word and len(word["analysis"]):
                    try:
                        word_lmd = word["analysis"][0]["lex"]
                        if len(self.speech_parts):
                            speech_part = re.split("[,=]+", word["analysis"][0]["gr"])
                            if speech_part[0] not in self.speech_parts:
                                continue
                        sentence_lmd.append(word_lmd)
                    except Exception:
                        self.logger.info("lemm error: {}".format(word))
                        pass
            if len(sentence_lmd):
                yield " ".join(sentence_lmd)

    def _preprocess(self, corpus):
        """Предобработка предложения - удаление символов"""
        for sentence in corpus:
            try:
                sentence = self.REPLACE_NO_SPACE.sub("", sentence)
            except:
                sentence = ""
            yield (sentence)











def preprocess(corpus):
    """Предобработка предложения - удаление символов"""
    for sentence in corpus:
        try:
            sentence = REPLACE_NO_SPACE.sub("", sentence)
        except:
            sentence = ""
        yield(sentence)


def lemm(corpus, speech_parts):
    for sentence in corpus:
        sentence_lmd = []
        result = lemmmatizator.analyze(sentence)
        for word in result:
            if "analysis" in word and len(word["analysis"]):
                try:
                    word_lmd = word["analysis"][0]["lex"]
                    if len(speech_parts):
                        speech_part = re.split("[,=]+", word["analysis"][0]["gr"])
                        if speech_part[0] not in speech_parts:
                            continue
                    sentence_lmd.append(word_lmd)
                except Exception:
                    logging.info("lemm error: {}".format(word))
                    pass
        if len(sentence_lmd):
            yield " ".join(sentence_lmd)


def get_ids_by_topic(topic_name):
    """Возвращает id семплов, которые формирут топик"""
    return np.where(_lda_keys == topic_name)

def get_smpls_by_topic(topic_name, count):
    """Возвращает id семплов, которые формирут топик"""
    res = np.array(corpus).take(get_ids_by_topic(topic_name))
    return res




if __name__ == "__main__":
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)


    n_records = 5000  # Количество записей для анализа из считанных файлов

    data = pd.DataFrame()

    files = ["dud-serebrakov-5000.csv"]

    for f in files:
        f_data = pd.read_csv("./comments/{}".format(f))
        print(f_data.shape)
        data = pd.concat([data, f_data], ignore_index=True)

        # Point0 - data loaded
    idx1 = random.sample(data.index.to_list(), n_records)
    corpus = data.iloc[idx1]["comment"].values


    tm = TopicModeller()
    tm.fit(corpus)
    tm.draw()
    """



    #-=Initilizing=-

    #Parameters
    n_topics = 9  # number of topics
    n_iter = 100  # number of iterations
    n_top_words = 8
    random_state = 111

    REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")

    n_records = 5000 # Количество записей для анализа из считанных файлов



    #Allowed speech parts
    speech_parts = {"A", "S", "V"}

    #Stopwords
    try:
        #f = open("stopwords.txt")
        with open("stopwords.txt") as f:
            stopwords = f.read().splitlines()
            logging.info("stopwords ok")
    except Exception as e:
        stopwords = ["а", "и", "я", "раз"]
        logging.warning(e, exc_info=True)

    #Frameworks
    lemmmatizator = Mystem()
    vectorizer = CountVectorizer(min_df=3, stop_words=stopwords, ngram_range=(1, 3))
    vectorizer2 = TfidfVectorizer(min_df=5, stop_words=stopwords, ngram_range=(1, 3))

    lda_model = lda.LDA(n_topics=n_topics, n_iter=n_iter, random_state=random_state)


    #Readings data



    data = pd.DataFrame()

    files = ["dud-serebrakov-5000.csv"]

    for f in files:
        f_data = pd.read_csv("./comments/{}".format(f))
        print(f_data.shape)
        data = pd.concat([data, f_data], ignore_index=True)

    #Point0 - data loaded
    idx1 = random.sample(data.index.to_list(), n_records)
    corpus = data.iloc[idx1]["comment"].values


    #Point1 - Preprocessing
    corpus = list(preprocess(corpus))
    corpus = list(lemm(corpus, speech_parts))


    #Point2 - Vectorization
    cvz = vectorizer.fit_transform(corpus)          #(samplex x features)

    #Point3 - LDA model
    X_topics = lda_model.fit_transform(cvz)         #(samples x topics)

    _lda_keys = []                                  #(samples x topic_num)
    for i in range(X_topics.shape[0]):
        _lda_keys += X_topics[i].argmax(),



    keywords = np.array(vectorizer.get_feature_names())
    topic_keywords = []                             #(topic x keywords)
    for topic_weights in lda_model.components_:
        top_keyword_locs = (-topic_weights).argsort()[:n_top_words]
        topic_keywords.append(keywords.take(top_keyword_locs))


    # Dimension reduction
    threshold = 0.75
    _idx = np.amax(X_topics, axis=1) > threshold  # idx of doc that above the threshold

    X_topics = X_topics[_idx]
    _lda_keys = np.array(_lda_keys)[_idx]

    tsne_model = TSNE(n_components=2, verbose=1, random_state=0, angle=.99, init='pca')
    tsne_lda = tsne_model.fit_transform(X_topics)



    topic_coord = np.empty((X_topics.shape[1], 2)) * np.nan
    print(_lda_keys)
    for _idx, topic_num in enumerate(_lda_keys):
        print(_idx)
        print(topic_num)
        topic_coord[topic_num] = tsne_lda[_idx]

        if not np.isnan(topic_coord).any():
            break

    fig = plt.figure()
    ax = plt.axes()

    ax.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], c=_lda_keys, alpha=0.3, )  # label=
    ax.set(title="Scatter plot example",
           xlabel='x', ylabel='y')

    for i, txt in enumerate(topic_keywords):
        ax.annotate(" \n".join(txt), (topic_coord[:, 0][i], topic_coord[:, 1][i]))
    plt.show()


 """

