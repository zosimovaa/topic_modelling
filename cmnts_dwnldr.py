import pandas as pd
import logging
import sys

config ={
    'api_key': 'AIzaSyD2kMwfHQImKFOdPVZLKko82NDULrUlbEQ',#'AIzaSyC2MG7FjBsCvtR7Xy6ptS_N5mfPpSpUFEA', #''AIzaSyD2kMwfHQImKFOdPVZLKko82NDULrUlbEQ',
    'api_url': "https://www.googleapis.com/youtube/v3/commentThreads?key={0}&textFormat=plainText&part=snippet&videoId={1}&maxResults={2}",
    'result_per_page': 100
}

works = [
    {
        "id":"XFqayVEIJwE",
        "name":"dud-davidych",
        "count": 100000
    },
    {
        "id": "_PbNqiWNlzQ",
        "name": "dud-ivleeva",
        "count": 100000
    },
    {
        "id": "7tKb5u52nSE",
        "name": "dud-nagiev",
        "count": 100000
    },
    {
        "id": "oo1WouI38rQ",
        "name": "dud-gulag",
        "count": 100000
    },
    {
        "id": "7yzZcAj24AI",
        "name": "dud-tinkof",
        "count": 100000
    },
    {
        "id": "7JrIAY5G7jE",
        "name": "dud-kiselev",
        "count": 100000
    },
    {
        "id": "CNJL4GsSrcc",
        "name": "dud-serebrakov",
        "count": 100000
    },
{
        "id": "5d9yXzsxe6w",
        "name": "d3-db11",
        "count": 100000
    },
{
        "id": "_KHbNA-_vqg",
        "name": "d3-rozygrysh",
        "count": 100000
    },
{
        "id": "fejt0x8C6OE",
        "name": "d3-g63_5.5m",
        "count": 100000
    },
    {
        "id": "BG4hJcFvs9I",
        "name": "d3_bmwm850",
        "count": 100000
    },
{
        "id": "h96JqnfBGyM",
        "name": "zrbn-japan5",
        "count": 100000
    },
{
        "id": "cReP8XVp1ww",
        "name": "zrbn-jumpinwall",
        "count": 100000
    }
]




from yce import YoutubeCommentExtractor
yce = YoutubeCommentExtractor(config)
logging.basicConfig(stream=sys.stdout, level=logging.INFO)


for work in works:
    print("=======================================================")
    res = yce.get_video_comments(work["id"], work["count"])
    res.to_csv("./comments_100k/{0}.csv".format(work["name"]))

print("done")
