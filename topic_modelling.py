# Imports
# ------------------------------------------------------------------------------
import re
import sys
import random
import logging

import numpy as np
import pandas as pd

from pymystem3 import Mystem
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.decomposition import LatentDirichletAllocation
import lda
from sklearn.manifold import TSNE
from matplotlib import pyplot as plt

# Constants and params
# ------------------------------------------------------------------------------
random_state = 11102

path = "./test_corp/"
files = ["dud-serebrakov-5000.csv"]                 # Список файлов для
files = ["kukuruza.csv", "nurofen.csv", "bmw.csv", "borsch.csv", "jesus.csv", "macbook.csv", "vov.csv"]
n_records = 5000                                    # Количество записей для анализа из считанных файлов (для уменьшения выборки)

REPLACE_NO_SPACE = re.compile("[.;:!\'?,\"()\[\]]")
speech_parts = {"A", "ADV", "S", "V"}               #Части речи, которые пропустит лемматизатор
vect_min_df = 3

lda_topics = 10                                      # number of topics
lda_iter = 100                                        # number of iterations
lda_top_words = 8

threshold = 0.6

# Functions defining
# ------------------------------------------------------------------------------
def txt_to_csv(fname):
    with open("{}.txt".format(fname)) as f:
        data = pd.DataFrame(f.read().splitlines())
        data.to_csv("{}.csv".format(fname))



def preprocess(c):
    """Предобработка предложения - удаление символов"""
    for sentence in c:
        try:
            sentence = REPLACE_NO_SPACE.sub("", sentence)
        except Exception as e:
            logging.warning(e)
            sentence = ""
        yield(sentence)


def lemm(c):
    for sentence in c:
        sentence_lmd = []
        result = lemmmatizator.analyze(sentence)
        for word in result:
            if "analysis" in word and len(word["analysis"]):
                try:
                    word_lmd = word["analysis"][0]["lex"]
                    if len(speech_parts):
                        speech_part = re.split("[,=]+", word["analysis"][0]["gr"])
                        if speech_part[0] not in speech_parts:
                            continue
                    sentence_lmd.append(word_lmd)
                except Exception:
                    logging.info("lemm error: {}".format(word))
                    pass
        #if len(sentence_lmd):
        yield " ".join(sentence_lmd)


def get_ids_by_topic(topic_name):
    """Возвращает id семплов, которые формирут топик"""
    return np.where(samples_keys == topic_name)

def get_smpls_by_topic(topic_name, count):
    """Возвращает id семплов, которые формирут топик"""
    res = np.array(corpus).take(get_ids_by_topic(topic_name))
    return res


print("\n------------------------------------------------------------------------------")
print("Point0 - Preparing")
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

#Stopwords
try:
    with open("stopwords.txt") as f:
        stopwords = f.read().splitlines()
        logging.info("stopwords ok")
except Exception as e:
    stopwords = ["а", "и", "я", "раз"]
    logging.warning(e, exc_info=True)

#Frameworks
lemmmatizator = Mystem()
vectorizer = CountVectorizer(min_df=vect_min_df,
                             stop_words=stopwords,
                             ngram_range=(1, 3))
lda_model = LatentDirichletAllocation(n_components=lda_topics,
                                      max_iter=lda_iter,
                                      random_state=random_state)
tsne_model = TSNE(n_components=2,
                  verbose=1,
                  random_state=random_state,
                  angle=.99,
                  init='pca')
lda_model = lda.LDA(n_topics=lda_topics, n_iter=lda_iter, random_state=random_state)

print("\n------------------------------------------------------------------------------")
print("Point1 - Reading data")
data = pd.DataFrame()



for f in files:
    f_data = pd.read_csv("./test_corp/{}".format(f))
    data = pd.concat([data, f_data], ignore_index=True)

idx = random.sample(data.index.to_list(), min(n_records, data.shape[0]))
corpus = data.iloc[idx]["comment"].values

#with open("test_corpus.txt") as f:
#    corpus = f.read().splitlines()






print("\n------------------------------------------------------------------------------")
print("Point2 - Corpus preprocessing")
corpus = list(preprocess(corpus))
corpus = list(lemm(corpus))


print("\n------------------------------------------------------------------------------")
print("Point3 - Vectorization and lda model fitting")


m_corpus = vectorizer.fit_transform(corpus)                 #(samplex x features)

samples_topics = lda_model.fit_transform(m_corpus)          # (samples x topics)

samples_keys = []                                           #(samples x topic_num)
for i in range(samples_topics.shape[0]):
    samples_keys.append(samples_topics[i].argmax())

keywords = np.array(vectorizer.get_feature_names())
topic_keywords = []                                         #(topic x keywords)
for topic_weights in lda_model.components_:
    top_keyword_locs = (-topic_weights).argsort()[:lda_top_words]
    topic_keywords.append(keywords.take(top_keyword_locs))


print("\n------------------------------------------------------------------------------")
print("Point4 - T-SNE and visualization")



_idx = np.amax(samples_topics, axis=1) > threshold  # idx of doc that above the threshold

samples_topics = samples_topics[_idx]
samples_keys = np.array(samples_keys)[_idx]


tsne_lda = tsne_model.fit_transform(samples_topics)

topic_coord = np.empty((samples_topics.shape[1], 2)) * np.nan

for _idx, topic_num in enumerate(samples_keys):
    topic_coord[topic_num] = tsne_lda[_idx]
    if not np.isnan(topic_coord).any():
        break

fig = plt.figure()
ax = plt.axes()

ax.scatter(x=tsne_lda[:, 0], y=tsne_lda[:, 1], c=samples_keys, alpha=0.3, )  # label=
ax.set(title="Scatter plot example",
       xlabel='x', ylabel='y')

for i, txt in enumerate(topic_keywords):
    ax.annotate(" \n".join(txt), (topic_coord[:, 0][i], topic_coord[:, 1][i]))
plt.show()

